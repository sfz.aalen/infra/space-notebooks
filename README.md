# Nix flake for space notebooks

This flake will be the system flake of the notebooks for the kids of the SFZ.

## Useful commands 

- nixos-rebuild switch --flake gitlab:sfz.aalen%2Finfra/space-notebooks#
- nixos-rebuild switch --flake gitlab:sfz.aalen%2Finfra/space-notebooks/COMMITID#

## Features

- [x] Automated install script with only manual secret copy
- [x] flake should pull updates from git automatically
- [x] needs GNOME desktop
- [x] sssd or ldap (either one is fine, use the one that works better)
- [x] rollback FS on boot (everything, even home directory)
- [x] mount NFS or SMB share on user login with KRB Ticket
> - [ ] centralized syslog -> canceled
- [ ] GIT-CI with automated ISO build
- [x] Admin login entry entfernen
- [x] Gnome wallpaper
- [ ] Non-admin offline user
- [ ] good admin password
- [ ] Doku
    - [ ] Quick install stuff

### Required user-space software

- [x] prusa slicer
- [x] firefox
- [ ] LEGO board thing
- [x] Arduino IDE

### Optional but really nice to have

- [x] boot wallpaper

## Installation

1. Boot into a nix 22.11 minimal ISO
> if you want to use WIFI in the minimal ISO 
> create config: `wpa_passphrase <SSID> <PW> | sudo tee /etc/wpa_supplicant.conf`
> activate it: `sudo wpa_supplicant -B -c /etc/wpa_supplicant.conf -i wlp3s0`
2. Run `nix run git+https://gitlab.com/sfz.aalen/infra/space-notebooks#install <disk-name> <hostname>`
> May require to use `--extra-experimental-features nix-command` and `--extra-experimental-features flakes`
3. Reboot
4. ??
5. Profit

## Adding a new notebook

1. add public key to `secrets/secrets.nix`
2. re-key secrets with `agenix -i <identity-file> -r`
> Use path to space-notebook-master identity

