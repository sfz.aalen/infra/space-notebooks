{
  description = "Space notbook system flake";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    impermanence.url = "github:nix-community/impermanence";
    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    { self
    , nixpkgs
    , nixpkgs-unstable
    , home-manager
    , nixos-hardware
    , treefmt-nix
    , impermanence
    , agenix
    }:
    let
      lib = nixpkgs.lib;
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
        config = { allowUnfree = true; };
      };
      pkgs-unstable = import nixpkgs-unstable {
        inherit system;
        config = { allowUnfree = true; };
      };
      treefmtEval = treefmt-nix.lib.evalModule pkgs ./treefmt.nix;
      user = "admin";
      uid = 999;
    in
    {
      formatter."${system}" = treefmtEval.config.build.wrapper;
      checks."${system}".formatter = treefmtEval.config.build.check self;
      packages."${system}".install = import ./install.nix { inherit pkgs user uid; };
      nixosConfigurations =
        let
          hosts = (import ./utils/hostnames.nix).all;
        in
        builtins.listToAttrs (builtins.map
          (hostname: {
            name = hostname;
            value = lib.nixosSystem {
              inherit system pkgs;
              specialArgs = { inherit pkgs-unstable; };
              modules = [
                nixos-hardware.nixosModules.lenovo-thinkpad-e14-amd
                impermanence.nixosModules.impermanence
                agenix.nixosModules.default
                ./modules
                {
                  space.network = {
                    inherit hostname;
                    hostid = import ./utils/hostId.nix hostname;
                  };
                  space.user.name = user;
                  space.user.uid = uid;
                }
                ./hardware/e15.nix
                home-manager.nixosModules.home-manager
                {
                  home-manager.useGlobalPkgs = true;
                  home-manager.useUserPackages = true;
                  home-manager.users."${user}" = import ./home;
                }
              ];
            };
          })
          hosts);
    };
}
