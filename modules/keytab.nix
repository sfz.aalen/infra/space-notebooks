{ pkgs, config, ... }:
let
  update-keytab = import ../pkgs/update_keytab.nix { inherit pkgs config; };
in
{
  systemd.services.copy_keytab = {
    enable = true;
    description = "update keytab";
    wantedBy = [ "multi-user.target" ];
    requires = [ "network-online.target" ];
    after = [ "network-online.target" ];
    unitConfig = {
      Type = "simple";
    };
    serviceConfig = {
      ExecStart = "${pkgs.bash}/bin/bash -c ${update-keytab}/bin/update_keytab";
    };
  };
}

