{ pkgs, config, ... }:
let
  copytool = import ../pkgs/fill_skel.nix {
    inherit pkgs;
    user = config.space.user.name;
  };
in
{
  services.sssd = {
    enable = true;
    config = ''
      [sssd]
      config_file_version = 2
      reconnection_retries = 3
      sbus_timeout = 30
      services = nss, pam, sudo
      domains = AD.SFZ-AALEN.SPACE

      [nss]
      reconnection_retries = 3

      [pam]
      reconnection_retries = 3

      [domain/AD.SFZ-AALEN.SPACE]
      # auth_provider = krb5
      access_provider = ad
      id_provider = ad
      krb5_realm = AD.SFZ-AALEN.SPACE
      krb5_server = ucs01.ad.sfz-aalen.space
      krb5_kpasswd = ucs01.ad.sfz-aalen.space
      krb5_store_password_if_offline = true
      krb5_keytab = /persist/krb5.keytab
      krb5_validate = false
      cache_credentials = true
      enumerate = true
      ad_domain = ad.sfz-aalen.space
      ldap_id_mapping = true
      override_shell = /run/current-system/sw/bin/bash
      override_homedir = /home/%u
    '';
  };
  #kerberos setup
  security.krb5 = {
    enable = true;
    settings = {
      libdefaults = {
        default_realm = "AD.SFZ-AALEN.SPACE";
        kdc_timesync = 1;
        ccache_type = 4;
        forwardable = true;
        proxiable = true;
        default_tkt_enctypes = "arcfour-hmac-md5 des-cbc-md5 des3-hmac-sha1 des-cbc-crc des-cbc-md4 des3-cbc-sha1 aes128-cts-hmac-sha1-96 aes256-cts-hmac-sha1-96";
        permitted_enctypes = "des3-hmac-sha1 des-cbc-crc des-cbc-md4 des-cbc-md5 des3-cbc-sha1 arcfour-hmac-md5 aes128-cts-hmac-sha1-96 aes256-cts-hmac-sha1-96";
        allow_weak_crypto = true;
        rdns = false;
      };
      realms = {
        "AD.SFZ-AALEN.SPACE" = {
          admin_server = "ucs01.ad.sfz-aalen.space";
          kpasswd_server = "ucs01.ad.sfz-aalen.space";
          kdc = [ "ucs01.ad.sfz-aalen.space" ];
        };
      };
    };
  };
  security.pam.makeHomeDir.skelDirectory = "/etc/skel";
  security.pam.services.login.makeHomeDir = true;
  security.pam.services.sshd.makeHomeDir = true;
  systemd.services.fill_skel = {
    enable = true;
    description = "copy configs to /etc/skel";
    wantedBy = [ "graphical.target" ];
    after = [ "home-manager-admin.service" ];
    unitConfig = {
      Type = "simple";
    };
    serviceConfig = {
      ExecStart = "${copytool}/bin/fill_skel";
    };
  };
}

