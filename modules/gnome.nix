{ pkgs, ... }:
{
  services.xserver = {
    enable = true;
    displayManager.gdm.enable = true;
    desktopManager.gnome.enable = true;
    xkb.layout = "de,us";
  };
  programs.dconf.enable = true;
  environment.gnome.excludePackages = (with pkgs; [
    gnome-tour
    cheese
    gnome-music
    gnome-maps
    epiphany
    geary
    gnome-characters
    gnome-weather
    gnome-contacts
  ]);
}
