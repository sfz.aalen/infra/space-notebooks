{ pkgs, pkgs-unstable, ... }: {
  environment.systemPackages = with pkgs; [
    curl
    vim
    tree
    git
    imagemagick

    libreoffice
    onlyoffice-bin
    gimp
    vlc

    (pkgs.inkscape-with-extensions.override {
      inkscapeExtensions = with pkgs-unstable.inkscape-extensions; [
        silhouette
        inkstitch
      ];
    })
    pstoedit

    freecad
    kicad
    openscad
    pkgs-unstable.dune3d
    blender
    arduino
    python312
    vscode
    sublime
    linphone
    prusa-slicer
    platformio
    python312Packages.jupyterlab
  ];

  programs.fuse.userAllowOther = true;
  environment.etc = {
    "machine-id".source = "/persist/etc/machine-id";
  };

  imports = [
    ./bluetooth.nix
    ./boot.nix
    ./domain.nix
    ./firefox.nix
    ./firmware.nix
    ./gnome.nix
    ./i18n.nix
    ./impermanence.nix
    ./keytab.nix
    ./network.nix
    ./nix.nix
    ./printing.nix
    ./secrets.nix
    ./shell.nix
    ./ssh.nix
    ./sudo.nix
    ./udev.nix
    ./update.nix
    ./user.nix
    ./intelliJ.nix
  ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11"; # Did you read the comment?
}

