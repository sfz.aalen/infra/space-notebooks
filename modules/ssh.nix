{
  services.openssh = {
    enable = true;
    knownHosts."10.20.42.25".publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBdXiT4L9Fn9FLmt/KYjDnoYe2Nyn6i11Da0TSzY8eyu";
  };
}
