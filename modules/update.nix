{ pkgs, ... }: {
  systemd.services.update_flake =
    let
      update_flake = import ../pkgs/update_flake.nix { inherit pkgs; };
    in
    {
      enable = true;
      description = "update system on boot";
      wantedBy = [ "multi-user.target" ];
      requires = [ "network-online.target" ];
      after = [ "network-online.target" ];
      unitConfig = {
        Type = "simple";
      };
      serviceConfig = {
        ExecStart = "${pkgs.bash}/bin/bash -c ${update_flake}/bin/update_flake";
      };
    };
}
