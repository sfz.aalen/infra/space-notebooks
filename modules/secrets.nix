{
  age.identityPaths = [ "/persist/etc/ssh/ssh_host_ed25519_key" ];
  age.secrets.keytab-access = {
    file = ../secrets/keytab-access.age;
  };
  age.secrets.obi-lan = {
    file = ../secrets/obi-lan.age;
  };
}
