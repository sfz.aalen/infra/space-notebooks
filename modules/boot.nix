{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot = {
    enable = true;
    configurationLimit = 1;
  };
  boot.loader.efi.canTouchEfiVariables = true;

  boot.plymouth = {
    enable = true;
    logo = builtins.fetchurl {
      url = "https://s3.sfz-aalen.space/static/laptop/bootup_logo.png";
      sha256 = "0ibajx3naz0cd08rmyhnc8bv259xx9ganhi3jvn1h7p88dqiy1ml";
    };
  };

  boot.supportedFilesystems = [
    "ntfs"
  ];
}
