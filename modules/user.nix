{ config, lib, ... }:
with lib;
let
  cfg = config.space.user;
in
{
  options.space.user = {
    name = mkOption {
      type = types.str;
    };
    uid = mkOption {
      type = types.number;
    };
  };
  config = {
    users.users."${cfg.name}" = {
      isSystemUser = true;
      group = "users";
      uid = cfg.uid;
      extraGroups = [ "wheel" "dialout" ];
      # generate hashed password with:
      # $ nix-shell --run 'mkpasswd -m SHA-512 -s' -p mkpasswd
      # initialHashedPassword = "...";
      initialPassword = cfg.name;

      createHome = true;
      home = "/home/${cfg.name}";
      useDefaultShell = true;
    };
  };
}
