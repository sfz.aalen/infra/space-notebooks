{ pkgs, ... }: {
  environment.shellAliases = {
    system-update = ''nixos-rebuild switch --flake git+https://gitlab.com/sfz.aalen/Infra/space-notebooks?ref=main'';
  };
}
