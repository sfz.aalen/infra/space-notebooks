{
  services.udev.extraRules = ''
    SUBSYSTEM=="tty", MODE="0777", GROUP="domain users"
    SUBSYSTEM=="usb", ATTR{idVendor}=="0b4d", ATTR{idProduct}=="1137" , MODE="0666", GROUP="domain users"
  '';
}
