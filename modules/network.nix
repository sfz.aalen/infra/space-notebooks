{ config, lib, ... }:
with lib;
let
  cfg = config.space.network;
in
{
  options.space.network = {
    hostname = mkOption {
      type = types.str;
    };
    hostid = mkOption {
      type = types.str;
      description = "something unique in the network";
    };
  };
  config = {
    networking = {
      hostName = cfg.hostname;
      hostId = cfg.hostid;
      networkmanager.enable = true;
      firewall.enable = true;
    };
    environment.etc = {
      "NetworkManager/system-connections/Obi LAN Kenobi.nmconnection" = {
        source = config.age.secrets.obi-lan.path;
        mode = "0600";
      };
    };
  };
}
