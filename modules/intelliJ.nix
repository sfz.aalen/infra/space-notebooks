{ pkgs, pkgs-unstable, ... }:

{
  environment.systemPackages = with pkgs-unstable; [
    jetbrains.idea-community-bin
  ];

  environment.etc = {
    "skel/.jdks/openjdk17".source = "${pkgs.jdk21_headless}/lib/openjdk";
    "skel/.jdks/openjdk21".source = "${pkgs.jdk17_headless}/lib/openjdk";
  };
  environment.sessionVariables = {
    LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath
      (with pkgs; [
        #put dependencies here :)
        libpulseaudio
        libGL
        glfw
        openal
        stdenv.cc.cc.lib
      ]);
  };
}
