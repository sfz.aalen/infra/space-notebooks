{ pkgs, ... }: {
  programs.firefox = {
    enable = true;
    package = pkgs.firefox-wayland;
    policies = {
      OverrideFirstRunPage = "";
      DisableSetDesktopBackground = true;
      DisableFirefoxAccounts = true;
      DisablePocket = true;
      NoDefaultBookmarks = true;
      DisplayBookmarksToolbar = "always";
      DisableProfileImport = true;
      ExtensionSettings = {
        "uBlock0@raymondhill.net" = {
          installation_mode = "force_installed";
          install_url = "https://addons.mozilla.org/firefox/downloads/latest/ublock-origin/latest.xpi";
        };
      };
      Bookmarks = [
        {
          Title = "StAArtpage";
          URL = "https://home.sfz-aalen.space/";
          Placement = "toolbar";
        }
        {
          Title = "Wiki";
          URL = "https://wiki.sfz-aalen.space/";
          Placement = "toolbar";
        }
      ];
      Homepage = {
        URL = "https://home.sfz-aalen.space/";
        Locked = false;
        StartPage = "homepage";
      };
      SearchEngines = {
        Default = "ddg";
      };
    };
  };
}
