{ pkgs, user, uid }: pkgs.writeShellApplication {
  name = "install";
  text = ''
    set -eo pipefail

    if [[ -z "$1" ]] || [[ -z "$2" ]]; then
        echo "usage: install <disk-name> <hostname>"
        exit 1
    else
        HOSTNAME="$2"
    fi

    if [ "$1" = nvme0n1 ]; then
        DISK="/dev/nvme0n1"
        PARTITION="/dev/nvme0n1p"
    elif [ "$1" = sda ]; then
        DISK="/dev/sda"
        PARTITION="$DISK"
    else
        echo "expected 'nvme0n1' or 'sda' as disk names"
        exit 1
    fi

    # create new table
    parted --script "$DISK" -- mklabel gpt

    # 0% automatically offsets for optimal performance
    parted --script "$DISK" -- mkpart ESP fat32 0% 512MB
    parted --script "$DISK" -- set 1 esp on

    # make some swap
    parted --script "$DISK" -- mkpart primary linux-swap 512MB 10GB

    # main BTRFS partition
    parted --script "$DISK" -- mkpart primary 10GB 100%

    # create BTRFS with all subvolumes
    mkfs.btrfs -f -L root "$PARTITION"3
    mount -t btrfs "$PARTITION"3 /mnt
    btrfs subvolume create /mnt/root
    btrfs subvolume create /mnt/nix
    btrfs subvolume create /mnt/persist
    btrfs subvolume create /mnt/log

    # take empty root snapshot
    btrfs subvolume snapshot -r /mnt/root /mnt/root-blank

    umount /mnt
    mount -o subvol=root,compress=zstd,noatime "$PARTITION"3 /mnt

    # setup dir to persist files
    mkdir /mnt/persist
    mount -o subvol=persist,compress=zstd,noatime "$PARTITION"3 /mnt/persist
    mkdir -p /mnt/persist/home/${user}
    chown ${builtins.toString uid}:100 /mnt/persist/home/${user}
    mkdir -p /persist/etc
    cp /etc/machine-id /persist/etc/machine-id
    mkdir -p /persist/etc/ssh
    ssh-keygen -f /persist/etc/ssh/ssh_host_ed25519_key -t ed25519 -q -N ""

    # create dirs and mounts for remaining subvolumes
    mkdir /mnt/nix
    mount -o subvol=nix,compress=zstd,noatime "$PARTITION"3 /mnt/nix
    mkdir -p /mnt/var/log
    mount -o subvol=log,compress=zstd,noatime "$PARTITION"3 /mnt/var/log

    # create and mount boot fs
    mkfs.fat -F 32 -n boot "$PARTITION"1
    mkdir /mnt/boot
    mount "$PARTITION"1 /mnt/boot

    # create swap
    mkswap -L swap "$PARTITION"2
    swapon "$PARTITION"2

    nixos-install --flake "git+https://gitlab.com/sfz.aalen/infra/space-notebooks#$HOSTNAME" --no-root-password
  '';
}
