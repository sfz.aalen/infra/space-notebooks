{
  xdg.configFile."PrusaSlicer/PrusaSlicer.ini".text = builtins.readFile ../resources/PrusaSlicer.ini;
  xdg.configFile."PrusaSlicer/physical_printer/SFZ Octoswarm.ini".text = builtins.readFile ../resources/Octoswarm.ini;
  xdg.configFile."PrusaSlicer/vendor/PrusaResearch.ini".text = builtins.readFile ../resources/PrusaResearch.ini;
  xdg.configFile."PrusaSlicer/printer/Original Prusa MINI & MINI+ Input Shaper.ini".text = builtins.readFile ../resources/InputShaper.ini;

}
