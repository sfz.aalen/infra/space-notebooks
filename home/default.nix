{ pkgs, osConfig, ... }:
let
  user = osConfig.space.user.name;
in
{
  home.stateVersion = "23.05";
  programs.home-manager.enable = true;
  home.username = user;
  home.homeDirectory = "/home/${user}";
  xdg.enable = true;

  home.packages = with pkgs; [
    killall
    git
    vim
  ];

  imports = [
    ./dconf.nix
    ./prusa-slicer.nix
  ];
}
