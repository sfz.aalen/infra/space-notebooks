{
  dconf.settings =
    let
      background = builtins.fetchurl {
        url = "https://s3.sfz-aalen.space/static/laptop/sfz_background_main1.png";
        sha256 = "1f4grs0904ylhs9an7nqb9siqbi2mmdah9w3vgx0yhqjkmcz8h17";
      };
    in
    {
      "org/gnome/desktop/peripherals/touchpad" = {
        tap-to-click = true;
        click-method = "default";
      };
      "org/gnome/settings-daemon/plugins/power" = {
        power-button-action = "interactive";
      };
      "org/gnome/desktop/interface" = {
        clock-show-seconds = true;
        clock-show-weekdays = true;
        color-scheme = "prefer-dark";
      };
      "org/gnome/shell" = {
        favorite-apps = [
          "firefox.desktop"
          "org.gnome.Nautilus.desktop"
          "org.gnome.Console.desktop"
        ];
      };
      "org/gnome/desktop/background" = {
        color-shading-type = "solid";
        picture-options = "zoom";
        picture-uri = "file://${background}";
        picture-uri-dark = "file://${background}";
        primary-color = "#3366ff";
        secondary-color = "#000000";
      };
      "org/gnome/desktop/wm/preferences" = {
        button-layout = "appmenu:minimize,maximize,close";
      };
      "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0" = {
        name = "terminal 1";
        command = "kgx";
        binding = "<Super>Return";
      };
      "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1" = {
        name = "terminal 2";
        command = "kgx";
        binding = "<Alt><Ctrl>t";
      };
      "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2" = {
        name = "explorer";
        command = "nautilus";
        binding = "<Super>e";
      };
      "org/gnome/settings-daemon/plugins/media-keys" = {
        custom-keybindings = [
          "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/"
          "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/"
          "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/"
        ];
      };
    };
  xdg.configFile."gtk-3.0/bookmarks".text = "file:///mnt/Lasercutter Lasercutter";
}
