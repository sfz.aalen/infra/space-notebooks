let
  padString = (n: if n < 10 then "0" + toString n else toString n);
in
with builtins; {
  all = genList (x: "TP${padString (x + 1)}") 10;
}
