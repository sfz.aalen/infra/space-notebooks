{ pkgs, user }: pkgs.writeShellApplication {
  name = "fill_skel";
  text =
    let
      mount_smb = import ./mount_smb.nix { inherit pkgs; };
    in
    ''
          ${pkgs.coreutils}/bin/mkdir -p /etc/skel/.config/{dconf,autostart,gtk-3.0}
          ${pkgs.coreutils}/bin/cp /home/${user}/.config/dconf/user /etc/skel/.config/dconf/
          ${pkgs.coreutils}/bin/cp /home/${user}/.config/gtk-3.0/* /etc/skel/.config/gtk-3.0
          ${pkgs.coreutils}/bin/cp -r /home/${user}/.config/PrusaSlicer /etc/skel/.config
          echo "${''
      [Desktop Entry]
      Type=Application
      Name=mountsmbshare
      Comment=iwantfiles
      Exec=${mount_smb}/bin/mount_smb
      OnlyShowIn=GNOME;
      X-GNOME-Autostart-Phase=Application
          ''}" > /etc/skel/.config/autostart/mount.desktop
    '';
}
