{ pkgs, config }: pkgs.writeScriptBin "update_keytab" ''
  ${pkgs.openssh}/bin/scp -i ${config.age.secrets.keytab-access.path} root@10.20.42.25:/etc/keytab.export /persist/krb5.keytab
  ${pkgs.systemd}/bin/systemctl restart sssd.service
''
