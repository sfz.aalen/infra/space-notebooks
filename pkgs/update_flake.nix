{ pkgs }: pkgs.writeShellApplication {
  name = "update_flake";
  runtimeInputs = [
    pkgs.git
    pkgs.nixos-rebuild
  ];
  text = ''
    nixos-rebuild boot --flake git+https://gitlab.com/sfz.aalen/Infra/space-notebooks?ref=main
  '';
}
