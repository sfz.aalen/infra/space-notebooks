{
  projectRootFile = "flake.nix";
  programs.nixpkgs-fmt.enable = true;
  programs.shellcheck.enable = true;
}
